var routes = [
  {path: "/", url: "index.html"},
  {path: "/detailfilm/", componentUrl: "pages/detail.html"},
  {path: "/cari/", componentUrl: "pages/cari.html"},
  {path: "/ttg/", url: "pages/tentang.html"},
  {path: "(.*)", url: "pages/404.html"}
];