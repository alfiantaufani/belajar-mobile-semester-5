var routes = [
  {path: "/", url: "index.html"},
  {path: "/tambahdata/", componentUrl: "pages/tambah.html"},
  {path: "/modifdata/", componentUrl: "pages/modif.html"},
  {path: "/gf1/", componentUrl: "pages/grafik1.html"},
  {path: "/gf/", componentUrl: "pages/grafik.html"},
  {path: "(.*)", url: "pages/404.html"}
];