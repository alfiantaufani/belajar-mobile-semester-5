var routes = [
  {path: "/", url: "index.html"},
  {path: "/tambah/", componentUrl: "pages/tambah.html"},
  {path: "/edit/", componentUrl: "pages/edit.html"},
  {path: "/ttg/", url: "pages/tentang.html"},
  {path: "(.*)", url: "pages/404.html"}
];